//Function to find car information by id
function carInformation(inventory, id) {
  if (!inventory) {
    return null;
  }

  //searching for car with id
  let [carDetails] = inventory.filter(function (car) {
    if (car.id === Number(id)) {
      return true;
    }
  });
  return carDetails;
}
//exporting the above function
module.exports = carInformation;
