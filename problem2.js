//Function to find latest car
function latestCar(inventory) {
  if (!inventory) {
    return null;
  }

  //returning last car
  let lastCar = inventory[inventory.length - 1];
  return lastCar;
}

//exporting the above function
module.exports = latestCar;
