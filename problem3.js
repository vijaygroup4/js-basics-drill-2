//Function for sorting cars based on model-Problem 3
function sortCarModels(inventory) {
  if (!inventory) {
    return null;
  }

  //creating carModels array
  let carModels = inventory.map((car) => {
    return car.car_model;
  });

  //sorting the car models into lexicographic order
  carModels.sort();
  return carModels;
}

//exporting the above function
module.exports = sortCarModels;
