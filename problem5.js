//Function to find cars older than 2000
function oldCars(inventory) {
  if (!inventory) {
    return null;
  }

  //filtering the cars
  let oldCarArray = inventory.filter((car) => {
    return car.car_year < 2000;
  });

  return oldCarArray;
}

//exporting the above function
module.exports = oldCars;
