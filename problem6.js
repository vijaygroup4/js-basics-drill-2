//Function to find bmw and Audi cars
function bmwAndAudiCars(inventory) {
  if (!inventory) {
    return null;
  }

  //filtering only BMW and Audi cars
  let bmwAndAudiArray = inventory.filter((car) => {
    return car.car_make === "BMW" || car.car_make === "Audi";
  });

  return bmwAndAudiArray;
}

//exporting the above function
module.exports = bmwAndAudiCars;
