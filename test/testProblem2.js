//destructuring latestCar function from problem2.js
let lastCar = require("../problem2");

//importing inventory array
let inventory = require("./inventory");

//Problem 2
let last_Car = lastCar(inventory);
if (last_Car) {
  console.log(`Last car is a ${last_Car.car_make} ${last_Car.car_model}`);
} else {
  console.log("Please provide a valid array");
}
