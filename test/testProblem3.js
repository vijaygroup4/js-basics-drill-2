//destructuring sortCarModels function from problem3.js
let sortCarModels = require("../problem3");

//importing inventory array
let inventory = require("./inventory");

//Problem 3
let carModels = sortCarModels(inventory);
if (carModels) {
  console.log(carModels);
} else {
  console.log("Please give valid array");
}
