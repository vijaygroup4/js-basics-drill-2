//destructuring oldCars function from problem5.js
let oldCars = require("../problem5");

//importing inventory array
let inventory = require("./inventory");

//Problem 5
let old_Cars = oldCars(inventory);

if (old_Cars) {
  console.log(old_Cars);
  console.log("Total number of old cars: " + old_Cars.length);
} else {
  console.log("Please give valid array");
}
