//destructuring bmwAndAudiCars function from problem6.js
let bmwAndAudiCars = require("../problem6");

//importing inventory array
let inventory = require("./inventory");

//Problem 6
let bmw_And_Audi_Cars = bmwAndAudiCars(inventory);
if (bmw_And_Audi_Cars) {
  console.log("BMW and Audi cars are:", JSON.stringify(bmw_And_Audi_Cars));
} else {
  console.log("Please give valid array);");
}
